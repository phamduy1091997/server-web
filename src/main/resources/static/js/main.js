jQuery(document).ready(function ($) {
    $("#checkInDate,#checkOutDate").dateTimePicker({
        paging: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
        picker: ['date'],
        format: 'Y-m-d',
        filter: function (date) {
            // Select date in the future
            var d = new Date();
            if (date.getTime() < d.getTime()) {
                return false;
            } else {
                return true;
            }
        },
        filter_show: function (date) {
            var d = new Date();
            return date.getYear() > d.getYear() || (date.getYear() == d.getYear() && date.getMonth() >= d.getMonth());
        }
    }).on('dp.change', function(e){ console.log(e.date); });

    $.validator.addMethod("validateCheckoutDate", function (value, element) {
        if (typeof (value) === 'undefined' || value === null) {
            return false;
        }
        var checkInDate = $("#checkInDate").val();
        if (typeof (checkInDate) === 'undefined' || checkInDate === null) {
            return true;
        }
        var checkoutDate = moment(value, 'YYYY-MM-DD');
        return moment(checkInDate, 'YYYY-MM-DD').isBefore(checkoutDate);
    }, "Checkout date must be after checkin date");

    $("#booking-form").validate({
        rules: {
            "checkInDate": {
                required: true
            },
            "checkOutDate": {
                required: true,
                validateCheckoutDate: true
            },
            "people": {
                required: true
            }
        },
        submitHandler: function (form) {
            form.submit();
        }

    });
});