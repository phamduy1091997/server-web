package com.server.web.service;

import com.server.web.entity.dao.User;

public interface UserService {
	User findUserByUserName(String userName);
}
