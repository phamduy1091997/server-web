package com.server.web.service;


import com.server.web.repo.CustomerRepository;
import com.server.web.repo.UserRepository;
import com.server.web.util.CryptoUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.HashMap;
import java.util.Map;

@Service
public class HashService {

    private static UserRepository userRepository;
    private static CustomerRepository customerRepository;

    @Autowired
    public HashService(UserRepository userRepositoryInstances, CustomerRepository customerRepositoryIntances) {
        HashService.userRepository = userRepositoryInstances;
        HashService.customerRepository = customerRepositoryIntances;
    }

    public static Map<String, String> encrypt(String rawData, String privateKey, String publicKey) throws Exception {
        String signature = CryptoUtil.signRSA(rawData, privateKey);
        String data = rawData + "|" + signature;
        byte[] byteKey = CryptoUtil.generateKey();

        String encryptData = CryptoUtil.encryptTripleDes(data, byteKey);

        String encryptKey = CryptoUtil.encryptRSA(byteKey, publicKey);
        Map<String, String> params = new HashMap<>();
        params.put("encryptData", encryptData);
        params.put("encryptKey", encryptKey);
        return params;
    }

    public static Map<String, String> decrypt(String encryptData, String encryptKey) throws Exception {
        Map<String, String> result = new HashMap<>();

        String partnerPrivateKey = userRepository.findUserByUserName("duy").getPartnerPrivateKey();
        String myCompanyPublicKey = customerRepository.findCustomerByUserName("duy").getMyCompanyPublicKey();

        byte[] decryptKey = CryptoUtil.decryptRSAToByte(encryptKey, partnerPrivateKey);

        String data = CryptoUtil.decryptTripleDes(encryptData, decryptKey);
        String checkData = data.substring(0, data.lastIndexOf("|"));
        String signature = data.substring(data.lastIndexOf("|") + 1);
        Boolean verify = CryptoUtil.verifyRSA(checkData, signature, myCompanyPublicKey);

        if (verify) {
            result.put("data", checkData);
            result.put("resultCode", "1");
            result.put("resultMessage", "Booking Room Successfully!");
            return result;
        } else {
            result.put("data", null);
            result.put("resultCode", "999");
            result.put("resultMessage", "Signature is invalid");
            return result;
        }
    }
}
