package com.server.web.controller;

import com.server.web.entity.EncryptRequest;
import com.server.web.entity.dao.User;
import com.server.web.repo.UserRepository;
import com.server.web.service.HashService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.StringHttpMessageConverter;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.client.RestTemplate;

import javax.servlet.http.HttpServletRequest;
import java.lang.reflect.Array;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Controller
@RequestMapping("/api")
@Slf4j
public class RequestPaymentController {
    @Autowired
    private HashService hashService;

    @Autowired
    private UserRepository userRepository;

    String urlPayment = "http://localhost:2401";

    @PostMapping("/request-payment")
    public ResponseEntity<?> requestPayment(@RequestBody EncryptRequest encryptRequest) throws Exception {
        RestTemplate restTemplate = new RestTemplate();
        String customerId = encryptRequest.getCustomerId();
        String accessCode = "1";
        String rawData = encryptRequest.getRawData();
        String data = accessCode + "|" + rawData;
        String privateKey = encryptRequest.getPrivateKey();
        String publicKey = encryptRequest.getPublicKey();

        Map params = HashService.encrypt(data, privateKey, publicKey);
        params.put("customerId", customerId);
        restTemplate.getMessageConverters().add(0, new StringHttpMessageConverter(StandardCharsets.UTF_8));
        HttpEntity<Map> paramBody = new HttpEntity<>(params);
        ResponseEntity<Map> returnResponse = restTemplate.postForEntity(this.urlPayment + "/api/payment",
                paramBody, Map.class);
        //decrypt
        Map<String, String> decryptResponse = new HashMap<String ,String>(returnResponse.getBody());
        Map<String, String> decryptResponseData;
        String encryptData = decryptResponse.get("encryptData");
        String encryptKey = decryptResponse.get("encryptKey");
        decryptResponseData = HashService.decrypt(encryptData, encryptKey);
        return ResponseEntity.ok(decryptResponseData);
    }


    @GetMapping("/item")
    public String PaymentPage(ModelMap map) {
        String privateKey = userRepository.findUserByUserName("duy").getPartnerPrivateKey();
        String publicKey = userRepository.findUserByUserName("duy").getMyCompanyPublicKey();
        map.addAttribute("public_key", publicKey);
        map.addAttribute("private_key", privateKey);
        return "PaymentPage";
    }

    @GetMapping("/login")
    public String loginPage() {
        return "LoginPage";
    }

}
