package com.server.web.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter

public class PaymentRequest {

    private String roomType;
    private Long price;

}
