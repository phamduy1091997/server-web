package com.server.web.entity.dao;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@Document(collection = "user")
public class User implements Serializable {
	@Id
	private String id;
	private String customerId;
	private String userName;
	private String passWord;
	private String partnerPrivateKey;
	private String myCompanyPublicKey;
}

