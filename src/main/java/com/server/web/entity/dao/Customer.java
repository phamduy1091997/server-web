package com.server.web.entity.dao;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.GenericGenerator;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.stereotype.Component;

import javax.persistence.GeneratedValue;

@Getter
@Setter
@Component
@Document(collection = "customer_auth")
public class Customer {

	@Id
	@GeneratedValue(generator = "system-uuid")
	@GenericGenerator(name = "system-uuid", strategy = "uuid")
	private String id;
	private String customerId;
	private String userName;
	private String partnerPrivateKey;
	private String myCompanyPublicKey;
	private long totalAmount;

}
