package com.server.web.entity;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.stereotype.Component;
import org.springframework.data.mongodb.core.mapping.Document;

@Getter
@Setter
@Component
@Document(collection = "web_server")
public class InfoPayments {
    @Id
    private String id;
    private long price;
}
