package com.server.web.entity;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.io.Serializable;

@Getter
@Setter
@Document(collection = "customer_auth")
public class User implements Serializable {

    @Id
    private String id;
    private String customerId;
    private String partnerPrivateKey;
    private String myCompanyPublicKey;
    private String username;
    private String password;

}
