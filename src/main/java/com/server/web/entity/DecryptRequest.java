package com.server.web.entity;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class DecryptRequest {
    private String encryptData;
    private String encryptKey;

    public String getEncryptData() {
        return encryptData;
    }

    public void setEncryptData(String encryptData) { this.encryptData = encryptData; }

    public String getEncryptKey() {
        return encryptKey;
    }

    public void setEncryptKey(String encryptKey) {
        this.encryptKey = encryptKey;
    }

}
