package com.server.web.repo;

import com.server.web.entity.dao.User;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface UserRepository extends MongoRepository<User, String> {
	User findUserByUserName(String userName);

}