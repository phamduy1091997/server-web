package com.server.web.repo;

import com.server.web.entity.dao.Customer;
import org.springframework.data.mongodb.repository.MongoRepository;

public interface CustomerRepository extends MongoRepository<Customer,String> {
	Customer findCustomerByUserName(String userName);
}
