package com.server.web.util;

import org.apache.commons.codec.binary.Base64;
import org.springframework.stereotype.Component;
import org.w3c.dom.Document;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import javax.crypto.Cipher;
import javax.crypto.KeyGenerator;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESedeKeySpec;
import javax.crypto.spec.SecretKeySpec;
import javax.xml.bind.DatatypeConverter;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.IOException;
import java.io.StringReader;
import java.math.BigInteger;
import java.security.KeyFactory;
import java.security.PrivateKey;
import java.security.PublicKey;
import java.security.Signature;
import java.security.spec.RSAPrivateCrtKeySpec;
import java.security.spec.RSAPublicKeySpec;

@Component
public class CryptoUtil {

    private static Cipher createCipherDecrypt(String xmlPrivateKey) throws Exception {
        PrivateKey privateKey = getPrivateKeyFromXML(xmlPrivateKey);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.DECRYPT_MODE, privateKey);
        return cipher;
    }
    private static Cipher createCipherEncrypt(String xmlPublicKey) throws Exception {
        PublicKey publicKey = getPublicKeyFromXML(xmlPublicKey);
        Cipher cipher = Cipher.getInstance("RSA");
        cipher.init(Cipher.ENCRYPT_MODE, publicKey);
        return cipher;
    }
    public static Document buildDocument(String xml) throws ParserConfigurationException, IOException, SAXException {
        DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
        DocumentBuilder builder = factory.newDocumentBuilder();
        InputSource is = new InputSource(new StringReader(xml));
        Document doc = builder.parse(is);

        return doc;
    }
    public static PrivateKey getPrivateKeyFromXML(String xml) throws Exception {
        RSAPrivateCrtKeySpec pkeyspec = null;
        Document doc = buildDocument(xml);
        BigInteger modulus = new BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("Modulus").item(0).getTextContent()));
        BigInteger publicExponent = new BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("Exponent").item(0).getTextContent()));
        BigInteger privateExponent = new
            BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("D").item(0).getTextContent()));
        BigInteger primeP = new BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("P").item(0).getTextContent()));
        BigInteger primeQ = new BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("Q").item(0).getTextContent()));
        BigInteger primeExponentP = new
            BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("DP").item(0).getTextContent()));
        BigInteger primeExponentQ = new
            BigInteger(1, DatatypeConverter.parseBase64Binary(doc.getElementsByTagName("DQ").item(0).getTextContent()));
        BigInteger crtCoefficient = new
            BigInteger(1, DatatypeConverter.parseBase64Binary(
            doc.getElementsByTagName("InverseQ").item(0).getTextContent()));
        pkeyspec = new
            RSAPrivateCrtKeySpec(modulus, publicExponent, privateExponent, primeP,
            primeQ, primeExponentP, primeExponentQ, crtCoefficient);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PrivateKey privKey = fact.generatePrivate(pkeyspec);
        return privKey;
    }
    public static PublicKey getPublicKeyFromXML(String xml) throws Exception {
        RSAPublicKeySpec pkeyspec = null;
        Document doc = buildDocument(xml);
        String modulus =
            doc.getElementsByTagName("Modulus").item(0).getTextContent();
        byte[] modulusBytes =
            DatatypeConverter.parseBase64Binary(modulus);
        BigInteger bigModulus = new BigInteger(1, modulusBytes);
        String exponent =
            doc.getElementsByTagName("Exponent").item(0).getTextContent();
        byte[] exponentBytes =
            DatatypeConverter.parseBase64Binary(exponent);
        BigInteger bigExponent = new BigInteger(1, exponentBytes);
        pkeyspec = new RSAPublicKeySpec(bigModulus, bigExponent);
        KeyFactory fact = KeyFactory.getInstance("RSA");
        PublicKey pubKey = fact.generatePublic(pkeyspec);
        return pubKey;
    }

    public static byte[] generateKey() throws Exception {
        // Get a key generator for Triple DES (a.k.a DESede)
        KeyGenerator keygen = KeyGenerator.getInstance("DESede");
        // Use it to generate a key
        SecretKey secretKey = keygen.generateKey();
        SecretKeyFactory keyfactory = SecretKeyFactory.getInstance("DESede");
        DESedeKeySpec keyspec = (DESedeKeySpec) keyfactory.getKeySpec(secretKey, DESedeKeySpec.class);
        byte[] rawkey = keyspec.getKey();
        return rawkey;
    }

    public static String encryptTripleDes(String data, byte[] key) throws Exception {
        // Create and initialize the encryption engine
        Cipher cipher = Cipher.getInstance("DESede");
        SecretKeySpec keyspec = new SecretKeySpec(key, "DESede");
        cipher.init(Cipher.ENCRYPT_MODE, keyspec);
        byte[] encBytes = cipher.doFinal(data.getBytes("UTF-8"));
        return Base64.encodeBase64URLSafeString(encBytes);
    }

    public static String decryptTripleDes(String data, byte[] key) throws Exception {
        // Create and initialize the encryption engine
        Cipher cipher = Cipher.getInstance("DESede");
        SecretKeySpec keyspec = new SecretKeySpec(key,
            "DESede");
        cipher.init(Cipher.DECRYPT_MODE, keyspec);
        byte[] decBytes = cipher.doFinal(Base64.decodeBase64(data));
        return new String(decBytes, "UTF-8");
    }

    public static String encryptRSA(byte[] byteData, String xmlPublicKey) throws Exception {
        Cipher cipher = createCipherEncrypt(xmlPublicKey);
        byte[] encrypted = cipher.doFinal(byteData);
        return Base64.encodeBase64URLSafeString(encrypted);
    }

    public static byte[] decryptRSAToByte(String encrypted, String xmlPrivateKey) throws Exception {
        Cipher cipher = createCipherDecrypt(xmlPrivateKey);
        byte[] bts = Base64.decodeBase64(encrypted);
        byte[] decrypted = cipher.doFinal(bts);
        return decrypted;
    }

    public static boolean verifyRSA(String data, String signature, String xmlPublicKey) throws Exception {
        PublicKey pubKey = getPublicKeyFromXML(xmlPublicKey);
        Signature instance = Signature.getInstance("SHA256withRSA");
        instance.initVerify(pubKey);
        instance.update(data.getBytes("UTF-8"));
        return instance.verify(Base64.decodeBase64(signature));
    }

    public static String signRSA(String data, String xmlPrivateKey) throws Exception {
        PrivateKey privKey = getPrivateKeyFromXML(xmlPrivateKey);
        Signature instance = Signature.getInstance("SHA256withRSA");
        instance.initSign(privKey);
        instance.update(data.getBytes("UTF-8"));
        byte[] signature = instance.sign();
        // convert Byte to Base64
        return Base64.encodeBase64String(signature);
    }

}
