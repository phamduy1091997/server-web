//package com.server.web.util;
//
//import com.server.web.entity.dao.User;
//import com.server.web.service.UserService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.security.authentication.AuthenticationProvider;
//import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
//import org.springframework.security.core.Authentication;
//import org.springframework.security.core.AuthenticationException;
//import org.springframework.security.core.GrantedAuthority;
//import org.springframework.security.core.authority.SimpleGrantedAuthority;
//import org.springframework.stereotype.Component;
//
//import java.util.ArrayList;
//import java.util.List;
//
//@Component
//public class CustomAuthenticationProvider implements AuthenticationProvider {
//
//	@Autowired
//	UserService userService;
//
//	@Override
//	public Authentication authenticate(Authentication authentication)
//			throws AuthenticationException {
//
//		String userName = authentication.getName();
//		String password = authentication.getCredentials().toString();
//
//		User user = userService.findUserByUserName(userName);
//
//		if (user != null) {
//			List<GrantedAuthority> authorities = new ArrayList<>();
//
//			authorities.add(new SimpleGrantedAuthority("USER"));
//
//			org.springframework.security.core.userdetails.User springSecurityUser =
//					new org.springframework.security.core.userdetails.User(userName, password, true, true, true, true, authorities);
//			return new UsernamePasswordAuthenticationToken(userName, password, authorities);
//		} else {
//			return null;
//		}
//	}
//
//	@Override
//	public boolean supports(Class<?> authentication) {
//		return authentication.equals(UsernamePasswordAuthenticationToken.class);
//	}
//
//	private List<GrantedAuthority> getGrantedAuthorities(List<String> privileges) {
//		List<GrantedAuthority> authorities = new ArrayList<>();
//		for (String privilege : privileges) {
//			authorities.add(new SimpleGrantedAuthority(privilege));
//		}
//		return authorities;
//	}
//}
